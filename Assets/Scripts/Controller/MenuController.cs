﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] private GameObject loadingPanel;
    [SerializeField] private GameObject settingsPanel;
    [SerializeField] private GameObject shopPanel;
    [SerializeField] private GameObject levelPanel;
    [SerializeField] private Animator menuPanel;
    [SerializeField] private Slider soundVolumeSlider;
    [SerializeField] private Slider musicVolumeSlider;
    [SerializeField] private Text coinsCountText;
    [SerializeField] private CanvasGroup pixelationToggle;
    [SerializeField] private ParticleSystem particle;

    [SerializeField] private LevelScript levelScript;

    public void Start()
    {
        loadingPanel.SetActive(false);

        Managers.Data.LoadGame();
        Managers.Audio.Play("Sound/Prologue");

        coinsCountText.text = "Coins Count: " + Managers.Coin.coin.ToString();

        particle.Play();
    }

    public void NewGame()
    {
        Managers.Level.FirstLevel();
    }

    public void SettingsOnOff()
    {
        if (settingsPanel.GetComponent<CanvasGroup>().alpha == 0)
        {
            menuPanel.SetTrigger("off");
            particle.Stop();
            settingsPanel.GetComponent<Animator>().SetTrigger("on");
            soundVolumeSlider.value = Managers.Audio.soundVolume;
            musicVolumeSlider.value = Managers.Audio.musicVolume;

            if (Managers.Level.pixelation == true)
                pixelationToggle.alpha = 1;
            else
                pixelationToggle.alpha = 0;
        }
        else
        {
            settingsPanel.GetComponent<Animator>().SetTrigger("off");
            menuPanel.SetTrigger("on");
            particle.Play();
            Managers.Data.SaveGameState();
        }
    }

    public void ShopOnOff()
    {
        if (shopPanel.GetComponent<CanvasGroup>().alpha == 0)
        {
            menuPanel.SetTrigger("off");
            particle.Stop();
            shopPanel.GetComponent<Animator>().SetTrigger("On");
            shopPanel.GetComponent<ShopController>().CheckCurrentHat();
        }
        else
        {
            shopPanel.GetComponent<Animator>().SetTrigger("Off");
            menuPanel.SetTrigger("on");
            particle.Play();
            Managers.Data.SaveGameState();
        }
    }

    public void LevelOnOff()
    {
        if (levelPanel.GetComponent<CanvasGroup>().alpha == 0)
        {
            menuPanel.SetTrigger("off");
            particle.Stop();
            levelScript.CheckUnlockLevel();
            levelPanel.GetComponent<Animator>().SetTrigger("On");
        }
        else
        {
            levelPanel.GetComponent<Animator>().SetTrigger("Off");
            menuPanel.SetTrigger("on");
            particle.Play();
            levelScript.UnchekUnlockLevel();
        }
    }


    public void Exit()
    {
        Managers.Data.SaveGameState();
        Application.Quit();
    }
}
