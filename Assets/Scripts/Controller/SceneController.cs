﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    float randomX;

    GameObject[] instObj = new GameObject[5];
    GameObject backGround;

    bool isReady = false;
    bool isEnd = false;

    [SerializeField] private GameObject obj;
    [SerializeField] private Text messageText;
    [SerializeField] private Text health;
    [SerializeField] private GameObject losePnael;
    [SerializeField] private Camera mainCamera;
    [SerializeField] private Camera pixelationCamera;

    float tmp = 0;

    public Text scoreText;

    private float time = 80;
    private float maxTime = 0;

    private int _score;

    public int score
    {
        get { return _score; }
        set { _score = value; }
    }

    private int goal = 30;

    [SerializeField] private Image timeBar;


    public void Awake()
    {
        Messenger.AddListener(GameEvent.LEVEL_COMPLETE, Win);
        Messenger.AddListener(GameEvent.LEVEL_FAILED, OnLevelFailed);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.LEVEL_COMPLETE, Win);
        Messenger.RemoveListener(GameEvent.LEVEL_FAILED, OnLevelFailed);
    }


    public void Start()
    {
        tmp = Managers.Audio.soundVolume;

        GameObject Background = GameObject.FindGameObjectWithTag("BackGround");

        if (Managers.Level.pixelation == true)
        {
            pixelationCamera.gameObject.SetActive(true);
            pixelationCamera.backgroundColor = Background.GetComponent<SpriteRenderer>().color;
            mainCamera.gameObject.SetActive(false);
        }
        else
        {
            mainCamera.gameObject.SetActive(true);
            mainCamera.backgroundColor = Background.GetComponent<SpriteRenderer>().color;

            pixelationCamera.gameObject.SetActive(false);
        }

        losePnael.SetActive(false);

        goal = goal + (Managers.Level.curLevel * 10);
        time = time + (Managers.Level.curLevel * 10);
        maxTime = time;

        Startup();
    }

    public void Startup()
    {
        scoreText.text = score.ToString() + "/" + goal.ToString();

        Managers.Audio.Play("Sound/Level" + Managers.Level.curLevel + "/" + "Music");

        StartCoundownTimer();

        for (int i = 0; i < 5; i++)
        {
            randomX = Random.Range(-8.5f, 8.5f);
            instObj[i] = Instantiate(obj, new Vector3(randomX, 6f, 0), Quaternion.identity);
            instObj[i].GetComponent<Renderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

            isReady = true;
        }
    }

    public void Update()
    {
        if (!isEnd)
        {
            health.text = "Health: " + Managers.Player.health.ToString();

            if (isReady == true)
                StartCoroutine(Spawn());

            if (time < 0)
            {
                Messenger.Broadcast(GameEvent.LEVEL_FAILED);
            }

            if (score >= goal)
            {
                Managers.Level.ReachObjective();
            }
        }
    }

    #region timer
    void StartCoundownTimer()
    {
      InvokeRepeating("UpdateTimer", 0.0f, 0.01667f);
    }

    void UpdateTimer()
    {
        time -= Time.deltaTime;
        timeBar.fillAmount = time / maxTime;  
    }
    #endregion

    public void UpdateScore()
    {
        scoreText.text = score.ToString() + "/" + goal.ToString();
    }

    public IEnumerator Spawn()
    {
        isReady = false;

        randomX = Random.Range(-8, 8.5f);
        GameObject newObject = Instantiate(obj, new Vector3(randomX, 6f, 0), Quaternion.identity);
        newObject.GetComponent<SpriteRenderer>().color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

        yield return new WaitForSeconds(0.5f);
        isReady = true;
    }

    #region win and fail
    public void Win()
    {
        StartCoroutine(CompleteLevel());
    }

    public IEnumerator CompleteLevel()
    {
        messageText.text = "You win!";

        CancelInvoke("UpdateTimer");
        isEnd = true;

        yield return new WaitForSeconds(2f);

        Managers.Coin.AddCoin(100);
        Managers.Coin.CoinsForPassing += 100;
        Managers.Data.SaveGameState();
        Managers.Level.GoToNext();
    }

    public void OnLevelFailed()
    {
        if (isEnd == false)
        {
            isEnd = true;
            health.text = "Health: " + "0";

            Managers.Audio.StopMusic();
            Managers.Audio.PlaySound(Resources.Load("Sound/Loose") as AudioClip);

            CancelInvoke("UpdateTimer");

            losePnael.transform.GetChild(1).GetComponent<Text>().text = "You got " + Managers.Coin.CoinsForPassing + " coins!";
            losePnael.SetActive(true);
        }
    }

    #endregion

    public void Restart()
    {
        Managers.Level.RestartCurrent();
    }
    public void ToMenu()
    {
        Managers.Coin.CoinsForPassing = 0;
        Managers.Data.SaveGameState();
        Managers.Level.GoToMenu();
    }
    public void OnApplicationQuit()
    {
        Managers.Audio.soundVolume = tmp;
        Managers.Data.SaveGameState();
    }
}
