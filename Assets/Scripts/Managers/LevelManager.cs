﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour, IGameManager
{
    public ManagerStatus status {get; private set;}

    public int openLevel { get; private set; }
    public int curLevel { get; private set; }
    public int maxLevel { get; private set; }

    private bool _pixelation;
    public bool pixelation
    {
        get { return _pixelation; }
        set { _pixelation = value; }
    }

    public void Startup()
    {
        Debug.Log("Player manager starting...");

        UpdateData(0, 5, 0, false);

        status = ManagerStatus.Started;
    }

    public void FirstLevel()
    {
        curLevel = 2;

        if(openLevel < 2)
            openLevel = 2;

        string name = "Level" + curLevel;
        Managers.Player.health = Managers.Player.maxHealth;
        SceneManager.LoadScene(name);
    }

    public void GoToLevel(int levelNumber)
    {
        string levelName = "Level" + levelNumber;
        curLevel = levelNumber;
        SceneManager.LoadScene(levelName);
        Managers.Player.health = Managers.Player.maxHealth;
    }

    public void GoToNext()
    {
        if (curLevel < maxLevel)
        {
            curLevel++;

            if (openLevel < curLevel)
                openLevel++;

            string name = "Level" + curLevel;
            Managers.Player.health = Managers.Player.maxHealth;
            SceneManager.LoadScene(name);
        }
        else
        {
            StartCoroutine(WinGame());
        }
    }

    public IEnumerator WinGame()
    {
        Text text = GameObject.FindGameObjectWithTag("MessageText").GetComponent<Text>();
        text.text = "You win the game!";

        yield return new WaitForSeconds(3f);

        GoToMenu();    
    }

    public void GoToMenu()
    {
       curLevel = 1;
       SceneManager.LoadScene(1);
       Managers.Player.health = Managers.Player.maxHealth;
    }


    public void RestartCurrent()
    {
        string name = "Level" + curLevel;
        SceneManager.LoadScene(name);
        Managers.Player.health = Managers.Player.maxHealth;
    }

    public void ReachObjective()
    {
        Messenger.Broadcast(GameEvent.LEVEL_COMPLETE);
    }

    public void UpdateData(int curLevel, int maxLevel, int openLevel, bool pixelation)
    {
        this.curLevel = curLevel;
        this.maxLevel = maxLevel;
        this.openLevel = openLevel;
        this.pixelation = pixelation;
    }
   
}
