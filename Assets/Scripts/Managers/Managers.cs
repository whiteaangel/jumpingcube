﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LevelManager))]
[RequireComponent(typeof(AudioManager))]
[RequireComponent(typeof(DataManager))]
[RequireComponent(typeof(CoinManager))]
[RequireComponent(typeof(PlayerManager))]
public class Managers : MonoBehaviour
{
    public static LevelManager Level { get; private set; }
    public static AudioManager Audio { get; private set; }
    public static DataManager Data { get; private set; }
    public static CoinManager Coin { get; private set; }
    public static PlayerManager Player { get; private set; }



    private List<IGameManager> _startSequence;

    private static Managers instance = null;

    public static Managers Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if(instance)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;

        DontDestroyOnLoad(gameObject);

        Level = GetComponent<LevelManager>();
        Audio = GetComponent<AudioManager>();
        Data = GetComponent<DataManager>();
        Coin = GetComponent<CoinManager>();
        Player = GetComponent<PlayerManager>();

        _startSequence = new List<IGameManager>();
        _startSequence.Add(Level);
        _startSequence.Add(Audio);
        _startSequence.Add(Coin);
        _startSequence.Add(Player);


        _startSequence.Add(Data);
     

        StartCoroutine(StartupManagers());
    }

    private IEnumerator StartupManagers()
    {
        foreach ( IGameManager manager in _startSequence)
        {
            manager.Startup();
        }

        yield return null;

        int numModules = _startSequence.Count;
        int numReady = 0;

        while(numReady < numModules)
        {
            int lastReady = numReady;
            numReady = 0;

            foreach(IGameManager manager in _startSequence)
            {
                if(manager.status == ManagerStatus.Started)
                {
                    numReady++;
                }
            }

            if (numReady > lastReady)
            {
                Debug.Log("Progress: " + numReady + "/" + numModules);
                Messenger<int, int>.Broadcast(StartupEvent.MANAGERS_PROGRESS, numReady, numModules);

            }
               
            yield return null;
        }

        Debug.Log("All started");
        Messenger.Broadcast(StartupEvent.MANAGERS_STARTED);
    }
}
