﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }

    private List<string> _purchasedHats = new List<string>();
    public List<string> purchasedHats
    {
        get { return _purchasedHats; }
        set { _purchasedHats = value; }
    }

    private int _health;
    public int health
    {
        get { return _health; }
        set { _health = value; }
    }

    private int _maxHealth;
    public int maxHealth
    {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }

    private float _jumpForce;

    public float jumpForce
    {
        get => _jumpForce;
        set => _jumpForce = value;
    }

    private int _damage;
    public int damage
    {
        get { return _damage; }
        set { _damage = value; }
    }

    private string _currentHat;
    public string currentHat
    {
        get { return _currentHat; }
        set { _currentHat = value; }
    }

    public void Startup()
    {
        Debug.Log("Player manager started..");

        purchasedHats.Add("cylinder");
        UpdateData(100, "cylinder", purchasedHats, 500f);

        status = ManagerStatus.Started;
    }

    public void HatSpawn(Transform parent)
    {
        Instantiate(Resources.Load("Hats/" + currentHat), parent.position,parent.rotation,parent);
    }

    public void UpdateData(int maxHealth, string currentHat, List<string> purchasedHats, float jumpForce)
    {
        this.health = maxHealth;
        this.maxHealth = maxHealth;
        this.currentHat = currentHat;
        this.jumpForce = jumpForce;

        this.purchasedHats.Clear();
        this.purchasedHats = purchasedHats;
    }
}
