﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DataManager : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }

    private string _filename;

    Scene scene;

    public void Startup()
    {
        Debug.Log("Data manager starting...");

        _filename = Path.Combine(Application.persistentDataPath, "game.dat");

        scene = SceneManager.GetActiveScene();

        status = ManagerStatus.Started;
    }

    public void SaveGameState()
    {
        Dictionary<string, object> gamestate = new Dictionary<string, object>();
        gamestate.Add("curLevel", Managers.Level.curLevel);
        gamestate.Add("maxLevel", Managers.Level.maxLevel);
        gamestate.Add("openLevel", Managers.Level.openLevel);
        gamestate.Add("pixelations", Managers.Level.pixelation);

        gamestate.Add("coin", Managers.Coin.coin);

        gamestate.Add("soundVolume", Managers.Audio.soundVolume);
        gamestate.Add("musicVolume", Managers.Audio.musicVolume);

        gamestate.Add("maxHealth", Managers.Player.maxHealth);
        gamestate.Add("currentHat", Managers.Player.currentHat);
        gamestate.Add("purchasedHats", Managers.Player.purchasedHats);
        gamestate.Add("jumpForce",Managers.Player.jumpForce);


        FileStream stream = File.Create(_filename);
        BinaryFormatter formatter = new BinaryFormatter();
        formatter.Serialize(stream, gamestate);
        stream.Close();
    }

    public void LoadGame()
    {
        if(!File.Exists(_filename))
        {
            Debug.Log("No saved games");
            return;
        }

        Dictionary<string, object> gamestate = new Dictionary<string, object>();

        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = File.Open(_filename,FileMode.Open);
        gamestate = formatter.Deserialize(stream) as Dictionary<string, object>;
        stream.Close();

        Managers.Level.UpdateData((int)gamestate["curLevel"], (int)gamestate["maxLevel"], (int)gamestate["openLevel"], (bool)gamestate["pixelations"]);

        Managers.Coin.UpdateData((int)gamestate["coin"]);

        Managers.Audio.UpdateData((float)gamestate["soundVolume"], (float)gamestate["musicVolume"]);

        Managers.Player.UpdateData((int)gamestate["maxHealth"], (string)gamestate["currentHat"], (List<string>)gamestate["purchasedHats"],(float)gamestate["jumpForce"]);

      //  if(scene.name != "Level1")
      // Managers.Level.RestartCurrent();  
    }
    
}
