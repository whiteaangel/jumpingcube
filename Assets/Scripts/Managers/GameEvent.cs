﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvent : MonoBehaviour
{
    public const string LEVEL_COMPLETE = "LEVEL_COMPLETE";
    public const string LEVEL_FAILED = "LEVEL_FAILED";
    public const string GAME_COMPLETE = "GAME_COMPLETE";
    public const string PLAYER_DAMAGED = "PLAYER_DAMAGED";
}
