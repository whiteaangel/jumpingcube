﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour, IGameManager
{
    public ManagerStatus status { get; private set; }

    private int _coin;
    private int _coinsForPassing;

    public int coin
    {
        get { return _coin; }
        set { _coin = value; }
    }

    public int CoinsForPassing
    {
        get { return _coinsForPassing; }
        set { _coinsForPassing = value; }
    }

    public void Startup()
    {
        Debug.Log("Coin manager started..");

        UpdateData(0);
        CoinsForPassing = 0;

        status = ManagerStatus.Started;
    }

    public void AddCoin(int coin)
    {
        this.coin += coin;
    }

    public void DeleteCoin(int coin)
    {
        this.coin -= coin;
    }

    public void UpdateData(int coin)
    {
        this.coin = coin;
    }
}
