﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
{
    [SerializeField] private CanvasGroup pixelationCheck;

    //Выключить звук ффектов
    public void OnSoundToggle()
    {
        Managers.Audio.soundMute = !Managers.Audio.soundMute;
    }

    public void OnSoundValue(float volume)
    {
        Managers.Audio.soundVolume = volume;
    }

    public void OnMusicToggle()
    {
        Managers.Audio.musicMute = !Managers.Audio.musicMute;
    }

    public void OnMusicValue(float volume)
    {
        Managers.Audio.musicVolume = volume;
    }

    public void PixelationOnOff()
    {
        if (Managers.Level.pixelation == false)
        {
            Managers.Level.pixelation = true;
            pixelationCheck.alpha = 1;
        }
        else
        {
            Managers.Level.pixelation = false;
            pixelationCheck.alpha = 0;
        }
    }

}
