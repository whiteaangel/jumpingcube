﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour
{
    [SerializeField] private GameObject UpBar;
    [SerializeField] private GameObject DownPanel;
    [SerializeField] private GameObject HatsPanel;
    [SerializeField] private CanvasGroup ErrorPanel;
    int Number = 0;
    string hatName;
    int hatCost = 0;

    void Start()
    {
        UpBar.transform.GetChild(Number).GetComponent<Animator>().SetTrigger("UpFix");

        int count = DownPanel.transform.childCount;
        for(int i = 1; i < count; i++)
        {
            DownPanel.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Off");
        }
    }

    public void AddNumber(int _number)
    {
        Number = 0;
        Number = _number;
    }

    public void GetHatName(string name)
    {
        hatName = name; 
    }

    public void GetHatCost(int cost)
    {
        hatCost = cost;
    }

    public void BuyHats()
    {
        if (Managers.Player.purchasedHats.Contains(hatName)) 
        {
            return;
        }

        if (Managers.Coin.coin >= hatCost)
        {
            Managers.Coin.coin -= hatCost;
            Managers.Player.purchasedHats.Add(hatName);
            HatsPanel.transform.GetChild(Number).GetChild(2).GetComponent<Text>().text = hatName;
        }
        else
        {
            StartCoroutine(Whait());
        }
    }

    IEnumerator Whait()
    {
        ErrorPanel.alpha = 1;
        yield return new WaitForSeconds(2f);
        ErrorPanel.alpha = 0;
    }

    public void SelectHat()
    {
        if (Managers.Player.purchasedHats.Contains(hatName))
        {
            Managers.Player.currentHat = hatName;
            for (int i = 0; i < HatsPanel.transform.childCount; i++)
            {
                HatsPanel.transform.GetChild(i).GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 0);
            }
            HatsPanel.transform.GetChild(Number).GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 1);
        }
    }

    public void SwitchPanel()
    {
        PanelOff();
        UpBar.transform.GetChild(Number).GetComponent<Animator>().SetTrigger("UpFix");
        DownPanel.transform.GetChild(Number).GetComponent<Animator>().SetTrigger("On");
    }

    public void PanelOff()
    {
        int count = DownPanel.transform.childCount;
        for (int i = 0; i < count; i++)
        {
            if(i != Number)
                UpBar.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Down");

            if (DownPanel.transform.GetChild(i).GetComponent<CanvasGroup>().alpha == 1)
                DownPanel.transform.GetChild(i).GetComponent<Animator>().SetTrigger("Off");
        }
    }

    public void CheckCurrentHat()
    {
        Number = 0;

        if (!Managers.Player.purchasedHats.Contains("cylinder"))
        {
            Managers.Player.purchasedHats.Add("cylinder");
        }

        for(int i = 0; i < Managers.Player.purchasedHats.Count; i++)
        {
            if (Managers.Player.currentHat == Managers.Player.purchasedHats[i])
            {
                Number = i;
            }
        }
        HatsPanel.transform.GetChild(Number).GetChild(1).GetComponent<Image>().color = new Color(1, 1, 1, 1);
    }
}
