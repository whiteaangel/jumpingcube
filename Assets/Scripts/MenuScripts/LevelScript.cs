﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelScript : MonoBehaviour
{
    [SerializeField] private GameObject BotomPanel;

    public void CheckUnlockLevel()
    {
        for (int i = Managers.Level.openLevel-1; i < Managers.Level.maxLevel - 1; i++)
        {
            BotomPanel.transform.GetChild(i).GetComponent<CanvasGroup>().interactable = false;
            BotomPanel.transform.GetChild(i).GetComponent<CanvasGroup>().blocksRaycasts = false;
            Color color = BotomPanel.transform.GetChild(i).GetChild(2).GetComponent<Image>().color;
            BotomPanel.transform.GetChild(i).GetChild(2).GetComponent<Image>().color = new Color(color.r, color.g, color.b, 1f);
        }

        BotomPanel.transform.GetChild(0).GetComponent<CanvasGroup>().interactable = true;
        BotomPanel.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = true;
        Color _color = BotomPanel.transform.GetChild(0).GetChild(2).GetComponent<Image>().color;
        BotomPanel.transform.GetChild(0).GetChild(2).GetComponent<Image>().color = new Color(_color.r, _color.g, _color.b, 0f);
    }

    public void UnchekUnlockLevel()
    {
        StartCoroutine(Whait());
    }

    IEnumerator Whait()
    {
        yield return new WaitForSeconds(1f);

        for (int i = Managers.Level.openLevel - 1; i < Managers.Level.maxLevel - 1; i++)
        {
            BotomPanel.transform.GetChild(i).GetComponent<CanvasGroup>().interactable = true;
            BotomPanel.transform.GetChild(i).GetComponent<CanvasGroup>().blocksRaycasts = true;
            Color color = BotomPanel.transform.GetChild(i).GetChild(2).GetComponent<Image>().color;
            BotomPanel.transform.GetChild(i).GetChild(2).GetComponent<Image>().color = new Color(color.r, color.g, color.b, 0f);
        }
    }

    public void SelectLevel(int number)
    {
        Managers.Level.GoToLevel(number);
    }

}
