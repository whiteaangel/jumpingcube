﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIText : MonoBehaviour
{
    public void OnPointerEnter()
    {
        this.gameObject.GetComponent<Text>().fontSize += 3;

        if(this.gameObject.GetComponent<Animator>() != null)
            this.gameObject.GetComponent<Animator>().SetBool("enter", true);

        Managers.Audio.PlaySound(Resources.Load("Sound/UIEffect/MenuClick") as AudioClip);
    }

    public void OnPointerExit()
    {
        this.gameObject.GetComponent<Text>().fontSize -= 3;

        if (this.gameObject.GetComponent<Animator>() != null)
            this.gameObject.GetComponent<Animator>().SetBool("enter", false);
    }
}
