﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    private Color color;
    public void OnDamaged()
    {
        color = this.GetComponent<SpriteRenderer>().color;
        this.GetComponent<SpriteRenderer>().color = new Color(0.9137255f,0.1529412f,0.2005119f, 1);
        StartCoroutine((BackColor()));
    }

    private IEnumerator BackColor()
    {
        yield return new WaitForSeconds(1f);
        this.GetComponent<SpriteRenderer>().color = color;
    }
}
