﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlapingController : MonoBehaviour
{
    SceneController _controller;
    [SerializeField] private AudioSource soundSource;
    [SerializeField] private SpriteRenderer cubeSprite;
    [SerializeField] private SpriteRenderer leftHand;
    [SerializeField] private SpriteRenderer rightHand;
    void Start()
    {
        _controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<SceneController>();
    }
    
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Object"))
        {
            soundSource.PlayOneShot(Instantiate(Resources.Load("Sound/Blop")) as AudioClip);

            Color color = collision.GetComponent<SpriteRenderer>().color;

            if (!(color == new Color(0.934f, 0.934f, 0.934f, 1)))
            {
                cubeSprite.color = new Color(color.r,color.g,color.b, 1);
                leftHand.color = new Color(color.r,color.g,color.b, 1);
                rightHand.color = new Color(color.r,color.g,color.b, 1);
            }
            _controller.score += 1; 
            _controller.UpdateScore();
        }
    }
}
