﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class PlayerController: MonoBehaviour
{
    public MovementController2D movementController;

    public float runSpeed = 40f;
    public float attackRange = .5f;
    public LayerMask enemyLayer;
    
    [SerializeField] private Canvas canvas;
    [SerializeField] private Transform hatSpawn;
    [SerializeField] private ParticleSystem boomEffect;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform rightHand;
    
    private float _horizontalMove = 0f;
    private bool _jump = false;
    private bool _isDead = false;
    
    private void GetDamage()
    {
        canvas.transform.GetChild(0).GetComponent<Text>().text = Managers.Player.damage.ToString();
        animator.SetBool("IsDamaged",true);
        StartCoroutine(AfterDamage());
    }
    IEnumerator AfterDamage()
    {
        yield return new WaitForSeconds(1f);
        animator.SetBool("IsDamaged",false);
        canvas.transform.GetChild(0).GetComponent<Text>().text = "";
        canvas.transform.GetChild(0).GetComponent<CanvasGroup>().alpha = 0;
        canvas.transform.GetChild(0).GetComponent<CanvasGroup>().interactable = false;
        canvas.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    private IEnumerator WhaitForDead()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
    
    private void Awake()
    {
        Messenger.AddListener(GameEvent.PLAYER_DAMAGED, GetDamage);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.PLAYER_DAMAGED,GetDamage);
    }

    public void Start()
    {
        _isDead = false;
        Managers.Player.HatSpawn(hatSpawn);
    }
    void Update()
    {
        if(this.transform.position.y <= -5 && _isDead == false)
        {
            _isDead = true;
            Managers.Player.health = 0;
            Messenger.Broadcast(GameEvent.LEVEL_FAILED);
        }

        if (Managers.Player.health <= 0 && _isDead == false)
        {
            _isDead = true;

            Color color = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
            var main = boomEffect.main;
            main.startColor = color;
            boomEffect.Play();

            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0);
            this.gameObject.GetComponent<Rigidbody2D>().simulated = false;

            for (int i = 0; i < 3; i++)
            {
                transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(false);
            }

            StartCoroutine(WhaitForDead());

            Messenger.Broadcast(GameEvent.LEVEL_FAILED);
        }
        
        _horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        animator.SetFloat("Speed",Mathf.Abs(_horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            _jump = true;
            animator.SetBool("IsJumping",true);
            StartCoroutine((OnLandingNumerator()));
        }

        if(Input.GetKeyDown(KeyCode.F))
        {
            Attack();
        }
    }
    private void Attack()
    { 
        animator.SetTrigger("Attack");
       Collider2D[] hitEnemies =  Physics2D.OverlapCircleAll(rightHand.position, attackRange, enemyLayer);
       foreach (var enemy in hitEnemies)
       {
           Debug.Log(("We hit " + enemy.name));
       }
    }
    private IEnumerator OnLandingNumerator()
    {
        yield return new WaitForSeconds(.5f);
        animator.SetBool("IsJumping",false);
    }

    private void FixedUpdate()
    {
        movementController.Move(_horizontalMove * Time.fixedDeltaTime, _jump);
        _jump = false;
    }
}
