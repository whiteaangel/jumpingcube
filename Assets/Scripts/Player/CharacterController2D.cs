﻿using SpriteGlow;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider2D))]
public class CharacterController2D : MonoBehaviour
{
    [SerializeField, Tooltip("Max speed, in units per second, that the character moves.")]
    float speed = 5;

    [SerializeField, Tooltip("Acceleration while grounded.")]
    float walkAcceleration = 35;

    [SerializeField, Tooltip("Acceleration while in the air.")]
    float airAcceleration = 15;

    [SerializeField, Tooltip("Deceleration applied when character is grounded and not attempting to move.")]
    float groundDeceleration = 45;

    [SerializeField, Tooltip("Max height the character will jump regardless of gravity")]
    float jumpHeight = 1.5f;

    [SerializeField] private AudioSource soundSource;
    [SerializeField] private Canvas Canvas;
    [SerializeField] private Transform hatSpawn;
    [SerializeField] private ParticleSystem BoomEffect;
    [SerializeField] private Animator Animator;

    private BoxCollider2D boxCollider;

    private Vector2 velocity;

    SceneController controller;

    private bool grounded;
    private bool isDead = false;

    private void Awake()
    {      
        boxCollider = GetComponent<BoxCollider2D>();
        Messenger.AddListener(GameEvent.PLAYER_DAMAGED, GetDamage);
    }

    private void OnDestroy()
    {
        Messenger.RemoveListener(GameEvent.PLAYER_DAMAGED,GetDamage);
    }

    public void Start()
    {
        isDead = false;
        Managers.Player.HatSpawn(hatSpawn);
        controller = GameObject.FindGameObjectWithTag("Controller").GetComponent<SceneController>();
        Debug.Log(controller);
    }

    private void GetDamage()
    {
        Canvas.transform.GetChild(0).GetComponent<Text>().text = Managers.Player.damage.ToString();
        Animator.SetTrigger("GetDamage");
        StartCoroutine(AfterDamage());
    }
    IEnumerator AfterDamage()
    {
        yield return new WaitForSeconds(1f);
        Canvas.transform.GetChild(0).GetComponent<Text>().text = "";
        Canvas.transform.GetChild(0).GetComponent<CanvasGroup>().alpha = 0;
        Canvas.transform.GetChild(0).GetComponent<CanvasGroup>().interactable = false;
        Canvas.transform.GetChild(0).GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Object"))
        {
            soundSource.PlayOneShot(Instantiate(Resources.Load("Sound/Blop")) as AudioClip);

            Color color = collision.GetComponent<SpriteRenderer>().color;

            if (!(color == new Color(0.934f, 0.934f, 0.934f, 1)))
            {
                transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(color.r,color.g,color.b, 1);
            }
            Debug.Log(controller.score);
            controller.score += 1; 
            controller.UpdateScore();
        }
    }

    private IEnumerator WhaitForDead()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }

    private void Update()
    {
        if(this.transform.position.y <= -5 && isDead == false)
        {
            isDead = true;
            Managers.Player.health = 0;
            Messenger.Broadcast(GameEvent.LEVEL_FAILED);
        }

        if (Managers.Player.health <= 0 && isDead == false)
        {
            isDead = true;

            Color color = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color;
            var main = BoomEffect.main;
            main.startColor = color;
            BoomEffect.Play();

            transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0);
            this.gameObject.GetComponent<Rigidbody2D>().simulated = false;

            for (int i = 0; i < 3; i++)
            {
                transform.GetChild(0).transform.GetChild(i).gameObject.SetActive(false);
            }

            StartCoroutine(WhaitForDead());

            Messenger.Broadcast(GameEvent.LEVEL_FAILED);
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            Animator.SetTrigger("jump");
        }

        // Use GetAxisRaw to ensure our input is either 0, 1 or -1.
        float moveInput = Input.GetAxisRaw("Horizontal");

        if (grounded)
        {
            velocity.y = 0;

            if (Input.GetButtonDown("Jump"))
            {
                // Calculate the velocity required to achieve the target jump height.
                velocity.y = Mathf.Sqrt(2 * jumpHeight * Mathf.Abs(Physics2D.gravity.y));
            }
        }

        float acceleration = grounded ? walkAcceleration : airAcceleration;
        float deceleration = grounded ? groundDeceleration : 0;

        if (moveInput != 0)
        {
            velocity.x = Mathf.MoveTowards(velocity.x, speed * moveInput, acceleration * Time.deltaTime);
        }
        else
        {
            velocity.x = Mathf.MoveTowards(velocity.x, 0, deceleration * Time.deltaTime);
        }

        velocity.y += Physics2D.gravity.y * Time.deltaTime;

        transform.Translate(velocity * Time.deltaTime);

        grounded = false;

        // Retrieve all colliders we have intersected after velocity has been applied.
        Collider2D[] hits = Physics2D.OverlapBoxAll(transform.position, boxCollider.size, 0);

        foreach (Collider2D hit in hits)
        {
            // Ignore our own collider.
            if (hit == boxCollider)
                continue;

            ColliderDistance2D colliderDistance = hit.Distance(boxCollider);

            // Ensure that we are still overlapping this collider.
            // The overlap may no longer exist due to another intersected collider
            // pushing us out of this one.
            if (colliderDistance.isOverlapped)
            {
                transform.Translate(colliderDistance.pointA - colliderDistance.pointB);

                // If we intersect an object beneath us, set grounded to true. 
                if (Vector2.Angle(colliderDistance.normal, Vector2.up) < 90 && velocity.y < 0)
                {
                    grounded = true;
                }
            }
        }
    }
}
