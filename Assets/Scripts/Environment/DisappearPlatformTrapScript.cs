﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisappearPlatformTrapScript : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(DisappearPlatformOff());
        }
    }

    IEnumerator DisappearPlatformOff()
    {
        yield return new WaitForSeconds(1f);

        Color color = this.gameObject.GetComponent<SpriteRenderer>().color;
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0);

        this.gameObject.GetComponent<Rigidbody2D>().simulated = false;

        StartCoroutine(DisappearPlatformOn());
    }

    IEnumerator DisappearPlatformOn()
    {
        yield return new WaitForSeconds(3f);

        Color color = this.gameObject.GetComponent<SpriteRenderer>().color;
        this.gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 1);

        this.gameObject.GetComponent<Rigidbody2D>().simulated = true;
    }
}
