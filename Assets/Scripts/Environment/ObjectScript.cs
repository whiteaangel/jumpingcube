﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour
{
    [SerializeField] private ParticleSystem BoomEffect;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Color color = this.gameObject.GetComponent<SpriteRenderer>().color;
            this.gameObject.GetComponent<SpriteRenderer>().color = new Color(color.r, color.g, color.b, 0);

            this.gameObject.GetComponent<Rigidbody2D>().simulated = false;

            var main = BoomEffect.main;
            main.startColor = color;
            BoomEffect.Play();

            StartCoroutine(WhaitForDead());
        }
    }

    public void Start()
    {
        StartCoroutine(LifeTime());
    }

    public void Update()
    {
        if (this.gameObject.transform.position.y < -5f)
            Destroy(this.gameObject);
    }

    public IEnumerator WhaitForDead()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }

    public IEnumerator LifeTime()
    {
        yield return new WaitForSeconds(5f);
        Destroy(this.gameObject);
    }
}
