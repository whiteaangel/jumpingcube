﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikesPlatformTrapScript : MonoBehaviour
{
    public int Damage = 30;
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Managers.Player.health -= Damage;
            Managers.Player.damage = Damage;
            Messenger.Broadcast(GameEvent.PLAYER_DAMAGED);

            //collision.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 5), ForceMode2D.Impulse);
            //StartCoroutine(StopForce(collision.gameObject.GetComponent<Rigidbody2D>()));
        }
    }
    IEnumerator StopForce(Rigidbody2D rig)
    {
        yield return new WaitForSeconds(0.5f);
        rig.velocity = Vector2.zero;
    }
}
